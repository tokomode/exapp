//<debug>
Ext.Loader.setPath({
    'Ext': 'touch/src',
    'ExApp': 'app'
});
//</debug>

Ext.application({
    name: 'ExApp',

	controllers: ['MainController', 'Timer', 'MapController'],
    views: ['Main', 'Home', 'ParkingList', 'Detail', 'Timer', 'ParkingListNav', 'MapView', 'MapViewContainer'],
	stores: ['Parkings'],
	models: ['Parking'],
	profiles: ['Phone', 'Desktop'],
	
	// globals
	globals : {
		pageNumber: 1,
		currentLocation: null,	
		comp: null,
		map: null,
		markers: []	
	},
	
    launch: function() {
	    Ext.fly('appLoadingIndicator').destroy();
    },
    
    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    }
});
