Ext.define('ExApp.store.Parkings', {
	extend: 'Ext.data.Store',
	xtype: 'parkings',
	
	config: {
		model: 'ExApp.model.Parking',
		storeId: 'parkingStore',
		proxy: {
			type: 'ajax',
			// url: 'http://www1.toronto.ca/City_Of_Toronto/Information_&_Technology/Open_Data/Data_Sets/Assets/Files/greenPParking.json',
			url: 'resources/json/greenPParking.json',
			reader: {
				type: 'json',
				rootProperty: 'carparks'
			}
		},
		sorters: [
			{
				property: 'distance',
				direction: 'ASC'
			}
		],
		autoLoad: 'false'
	}
});