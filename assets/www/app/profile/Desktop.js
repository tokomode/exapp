Ext.define('ExApp.profile.Desktop', {
	extend: 'Ext.app.Profile',
	
	config: {
		name: 'Desktop',
		views: ['Main']
	},
	
	isActive: function() {
		return Ext.os.is.Desktop;
	},
	
	launch: function() {
        Ext.Viewport.add({
        	xclass: 'ExApp.view.desktop.Main'
        });
    }	
});