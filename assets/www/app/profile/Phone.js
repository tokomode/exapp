Ext.define('ExApp.profile.Phone', {
	extend: 'Ext.app.Profile',
	
	config: {
		name: 'Phone',
		views: ['Main']
	},
	
	isActive: function() {
		return Ext.os.is.Phone;
	},
	
	launch: function() {
        Ext.Viewport.add({
        	xclass: 'ExApp.view.phone.Main'
        });
    }	
});