Ext.define('ExApp.controller.MainController', {
	extend: 'Ext.app.Controller',
	
	requires: [
		'Ext.device.Geolocation',
		'Ext.MessageBox'
	],
	
	config: {
		refs: {
			nav: 'parkinglistnav'
		},
		control: {
			'parkinglist': {
				disclose: 'showDetail'
			}
		}
	},
	
	// launch: function() {
	// 	Ext.device.Geolocation.getCurrentPosition({
	// 		success: function(position) {
	// 		},
	// 		failure: function() {
	// 			// Ext.Msg.alert('Geolocation', 'Something went wrong!');
	// 		}
	// 	});	
	// },
	
	showDetail: function(list, record) {
		this.getNav().push({
			xtype: 'detail',
			//title: record.print(),
			title: record.data.address,
			data: record.data
		});
	}
});