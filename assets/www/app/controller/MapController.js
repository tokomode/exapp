var isEnd = false;
var numberPerPage = 10;
var totalCount = -1;
var selectedMarkerIndex = -1;

Ext.define('ExApp.controller.MapController', {
	extend: 'Ext.app.Controller',
	xtype: 'mapcontroller',
	
	config: {
		refs: {
			map: 'mapview',
			loadMore: 'button[action=loadMore]',
			refresh: 'button[action=refresh]',
			previous: 'button[action=previous]',
			next: 'button[action=next]',
		}, 
		control: {
			map: {
				maprender: 'showMap'
			},
			loadMore: {
				tap: 'loadMore'
			},
			refresh: {
				tap: 'refresh'
			},
			previous: {
				tap: 'previous'
			},
			next: {
				tap: 'next'
			}
		}
	}, // end of config
		
	// ************************************************************************
	// event handlers
	// ************************************************************************
	refresh: function (button, e, eOpts) {
		isEnd = false;
		ExApp.app.globals.pageNumber = 1;
		this.getCurrentLocation();
	}, // end of refresh
	
	loadMore: function (button, e, eOpts) {
		if (isEnd == true) {
			console.log('No more to load - 1');
			return;
		}
		if (ExApp.app.globals.pageNumber * numberPerPage > totalCount) {
			isEnd = true;
			console.log('No more to load - 2');
			return;
		}
		
		ExApp.app.globals.pageNumber += 1;
		console.log('loading more: page number: ' + ExApp.app.globals.pageNumber);
		
		this.loadParkings(ExApp.app.globals.pageNumber);
	}, // end of loadMore
	
	showMap: function(comp, map, eOpts) {
		ExApp.app.globals.comp = comp;
		ExApp.app.globals.map = map;
		
		this.getCurrentLocation();
		
		// Hide info box when tapping on the map
		google.maps.event.addListener(map, 'click', function() {
			// console.log('Clicked on the map.');
			if (comp.isOpenInfo) {
				selectedMarkerIndex = -1;
				comp.infoBox.close();
				comp.isOpenInfo = !comp.isOpenInfo;
			}
		});	
	}, // end of showMap	
	
	previous: function() {
		if (selectedMarkerIndex == -1) {
			return;
		}
		if (selectedMarkerIndex == 0) {
			return;
		} else {
			selectedMarkerIndex--; 	
		}
		google.maps.event.trigger(ExApp.app.globals.markers[selectedMarkerIndex], 'mouseup');
	}, // end of previous
	
	next: function() {
		if (selectedMarkerIndex == -1) {
			return;
		}
		if (selectedMarkerIndex == ExApp.app.globals.markers.length - 1) {
			return;
		} else {
			selectedMarkerIndex++; 	
		}
		google.maps.event.trigger(ExApp.app.globals.markers[selectedMarkerIndex], 'mouseup');
	}, // end of next
	
	// ************************************************************************
	// private methods
	// ************************************************************************
	clearOverlays: function() {
		var markers = ExApp.app.globals.markers;
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(null);
		}
	}, // end of clearOverlays
	
	getCurrentLocation: function() {
		var self = this;
		Ext.device.Geolocation.getCurrentPosition({
			success: function(position) {
				// set current location in global variable				
				ExApp.app.globals.currentLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				ExApp.app.globals.comp.setMapCenter(position.coords);
				
				new google.maps.Marker({
					position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
					map: ExApp.app.globals.map
				});
				
				self.loadParkings(1);
				
			},
			failure: function() {
				console.log('Failed to get current location.');
			}
		});
	}, // end of getCurrentLocation
	
	loadParkings: function(pageNumber) {
		var markerBounds = new google.maps.LatLngBounds();
		
		// clear previous markers
		this.clearOverlays();
		
		this.addMarkersWithPage(ExApp.app.globals.comp, ExApp.app.globals.map, markerBounds, pageNumber);
		
		// auto zoom level basen on markers
		// ExApp.app.globals.map.fitBounds(markerBounds);
		
	}, // end of loadParkings
	
	addMarkersWithPage: function(comp, map, markerBounds, pageNumber) {
		var parkingStore = Ext.getStore('parkingStore');
		
		
		parkingStore.clearFilter();
				
		parkingStore.load(function(records, operation, success) {
			console.log('2. success: records size: ' + records.length);
			if (success == false) {
				console.log('*****ERROR: failed to load store.');
				return;
			}

			totalCount = parkingStore.data.length;
			
			ExApp.app.globals.markers.length = 0;

			// fileting by distance to show only parking lots within the page number
			var maxDistance = parkingStore.data.items[pageNumber * numberPerPage].get('distance');
			parkingStore.filterBy(function(record) {
				if (record.get('distance') < maxDistance) {
					return true;
				} else {
					return false;
				}
			});
			
			parkingStore.each(function(carpark, index, length) {
				// numbering for order of distance
				carpark.data.xindex = index + 1;
				
				var markerPoint = new google.maps.LatLng(carpark.get('lat'), carpark.get('lng'))
				markerBounds.extend(markerPoint);
				
				var parkingMarker = new google.maps.Marker({
					position: markerPoint,
					map: map,
					carpark: carpark,
					icon: "resources/images/icon-pink-ball-32.png",
				});

				ExApp.app.globals.markers.push(parkingMarker);
				
				google.maps.event.addListener(parkingMarker, 'mouseup', function() {
					comp.isOpenInfo = true;
					selectedMarkerIndex = ExApp.app.globals.markers.indexOf(parkingMarker);
					
					var park = parkingMarker.carpark;
				
					boxText = document.createElement("div");
		            boxText.id =park.get('id');
					boxText.class = 'callout';
		            boxText.style.cssText = "border: 0px solid black; margin-top: 0px; padding: 5px; color: white; font-size: 11px";
		            boxText.innerHTML = '[' + park.get('xindex') + '] ' +park.get('address') + '<br />' + park.get('distance');
	
		            var myOptions = {
		                content: boxText
		                ,disableAutoPan: false
		                ,maxWidth: 0
		                ,pixelOffset: new google.maps.Size(-107, -80)
		                ,zIndex: null
		                ,boxStyle: {
		                    background: "url('resources/images/bg-callout.png') no-repeat",
		                    opacity: 0.85,
		                    width: "195px",
							height: "50px"
		                }
		                ,closeBoxMargin: "0px 0px 2px 2px"
		                ,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
		                ,infoBoxClearance: new google.maps.Size(1, 1)
		                ,isHidden: false
		                ,pane: "floatPane"
		                ,enableEventPropagation: false
		            };
						
					if (!comp.infoBox) {
						comp.infoBox = new InfoBox();
						
						google.maps.event.addListener(comp.infoBox, 'closeclick', function() {
							console.log('Info box will be closed.');
							if (comp.isOpenInfo) {
								selectedMarkerIndex = -1;
								comp.isOpenInfo = !comp.isOpenInfo;
							}
						});
					}	
					comp.infoBox.setOptions(myOptions);
					comp.infoBox.open(parkingMarker.map, this);
				});
		
			}); // end of parkingStore
		}); // end of load		
	} // end of addMarkers with page

})