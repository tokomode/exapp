Ext.define('ExApp.controller.Timer', {
	extend: 'Ext.app.Controller',
	
	requires: [
		'Ext.MessageBox'
	],
	config: {
		refs: {
			timer: 'timer'
		},
		control: {
			'button[action=set]': {
				tap: 'setTimer'
			},
			'button[action=reset]': {
				tap: 'resetTimer'
			}
		}
	},
	
	setTimer: function() {
		//console.log('set timer');
		var container = this.getTimer();
		var textField = container.getComponent('timerTextField');
		textField.setValue(new Date());
		Ext.Msg.alert('set timer.');
	},
	
	resetTimer: function() {
		//console.log('reset timer');
		var container = this.getTimer();
		var textField = container.getComponent('timerTextField');
		textField.setValue('');
	}
});