Ext.define('ExApp.view.Detail', {
	extend: 'Ext.Panel',
	xtype: 'detail',
	
	config: {
		styleHtmlContent: true,
		scrollable: 'vertical',
		tpl: [
			'<div><strong>Address:</strong> {address}</div>',
			'<div><strong>Facility Type:</strong> {lot_type}</div>',
			'<div><strong>Capacity:</strong> {capacity} Spaces</div>',
			'<div><strong>Height Restriction (Approximate):</strong> {max_height} meters</div>',			
			'<div><strong>Payment Options:</strong> {payment_options}</div>',
			'<div><strong>Payment Methods:</strong> {payment_methods}</div>',
			'<hr />',
			'<div><strong>Rate Information:</strong> {rate}</div>',
			'<div><strong>Rate Details:</strong> </div>',
			'<div>{rate_details}</div>'
		].join(' ')
	}
});