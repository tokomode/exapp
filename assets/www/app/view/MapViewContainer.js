Ext.define('ExApp.view.MapViewContainer', {
	extend: 'Ext.Panel',
	xtype: 'mapviewcontainer',
	
	requires: [
		// 'Ext.Toolbar'
	],
	
	config: {
		layout: 'fit',

		items: [
			{
				xtype: 'toolbar',
				docked: 'top',
				items: [
					{
						xtype: 'button',
						action: 'previous',
						text: 'Previous'
					},
					{
						xtype: 'button',
						action: 'next',
						text: 'Next'
					},
					{ 
						xtype: 'spacer'
					},
					{
						xtype: 'button',
						action: 'loadMore',
						text: 'Load More'
					},
					{
						xtype: 'button',
						action: 'refresh',
						text: 'Refresh'
					}
				]
			},
			{
				xtype: 'mapview'
			}
		]
	}
})