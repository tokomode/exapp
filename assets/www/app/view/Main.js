Ext.define('ExApp.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
        'ExApp.view.Home',
        'ExApp.view.ParkingList',
        'ExApp.view.Timer'
    ],
    
    config: {
        tabBarPosition: 'top',
		defaults: {
			style: 'background-color:#F00'
		},
		
        items: [
            {
                title: 'Home',
                iconCls: 'home',

                styleHtmlContent: true,
                scrollable: true,
				xtype: 'home'
            },
            {
                title: 'List',
                iconCls: 'organize',
				xtype: 'parkinglistnav'
            },
            // {
            // 	title: 'Map',
            // 	iconCls: 'maps',
            // 	xtype: 'mapview'
            // },
            {
            	title: 'Timer',
            	iconCls: 'time',
            	xtype: 'timer'
            }
        ]
    }
});
