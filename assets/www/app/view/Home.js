Ext.define('ExApp.view.Home', {
	extend: 'Ext.Panel',
	
	xtype: 'home',
	requires: [
        'Ext.TitleBar'
    ],
	
	config: {
        items: {
            docked: 'top',
            xtype: 'titlebar',
            title: 'Welcome to ExApp'
        },

        html: [
            '<img width="65%" src="http://staging.sencha.com/img/sencha.png" />',
            '<h1>Welcome to Sencha Touch</h1>',
            "<p>You're creating the Getting Started app. This demonstrates how ",
            "to use tabs, lists and forms to create a simple app</p>",
            '<h2>Sencha Touch 2</h2>'
        ].join("")	
	}
});