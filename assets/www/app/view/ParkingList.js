Ext.define('ExApp.view.ParkingList', {
	extend: 'Ext.dataview.List',
	xtype: 'parkinglist',
	
	config: {
		title: 'Green P Parkings',
		itemTpl: '<div><b>{xindex}.</b> {address}</div> <div>{rate}</div> <div>{lat}, {lng}: Distance: {distance}</div>',
		store: {
			xtype: 'parkings'
		},
		onItemDisclosure: true		
	}
});