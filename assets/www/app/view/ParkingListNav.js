Ext.define('ExApp.view.ParkingListNav', {
	extend: 'Ext.navigation.View',
	xtype: 'parkinglistnav',
	
	requires: [
		'ExApp.view.ParkingList',
		'ExApp.view.Detail'
	],
	
	config: {
		items: [
			{
				xtype: 'parkinglist'
			}
		]
	}
});