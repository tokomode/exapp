Ext.define('ExApp.view.MapView', {
	extend: 'Ext.Map',
	xtype: 'mapview',
	
	config: {
		useCurrentLocation: true,
		listeners: {
            maprender : function(comp, map){
	            new google.maps.Marker({
                    position: new google.maps.LatLng(this._geo.getLatitude(), this._geo.getLongitude()),
                    map: map
                });

    			var parkingStore = Ext.getStore('parkingStore');

				parkingStore.load(function(records, operation, success) {
					console.log('success: recordes size: ' + records.length);
					// var mapController = ExApp.app.getController('Map');
					
	   				parkingStore.each(function(carpark, index, length) {
	  					var parkingMarker = new google.maps.Marker({
							position: new google.maps.LatLng(carpark.get('lat'), carpark.get('lng')),
	  						map: map,
							carpark: carpark,
	   						icon: "icon-pink-ball-32.png",//'https://maps.google.com/mapfiles/kml/shapes/parking_lot_maps.png',						
	  					});


						google.maps.event.addListener(parkingMarker, 'mouseup', function() {
							var park = parkingMarker.carpark;
						
							boxText = document.createElement("div");
				            boxText.id =park.get('id');
							boxText.class = 'callout';
				            boxText.style.cssText = "border: 0px solid black; margin-top: 0px; padding: 5px; color: white; font-size: 11px";
				            boxText.innerHTML = park.get('address');
			
				            var myOptions = {
				                content: boxText
				                ,disableAutoPan: false
				                ,maxWidth: 0
				                ,pixelOffset: new google.maps.Size(-110, -80)
				                ,zIndex: null
				                ,boxStyle: {
				                    background: "url('bg-callout.png') no-repeat",
				                    opacity: 0.85,
				                    width: "195px",
									height: "50px"
				                }
				                ,closeBoxMargin: "10px 2px 2px 2px"
				                ,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
				                ,infoBoxClearance: new google.maps.Size(1, 1)
				                ,isHidden: false
				                ,pane: "floatPane"
				                ,enableEventPropagation: false
				            };
								
							if (!comp.infoBox) {
								comp.infoBox = new InfoBox();
							}	
							comp.infoBox.setOptions(myOptions);
							comp.infoBox.open(parkingMarker.map, this);
						});
				
					}); // end of parkingStore
				}); // end of load
				
				var data = parkingStore.getData();
				console.log('data size: ' + data.length);
				

			} // end of maprender
		} // end of listener
	},  // end of config
	
	loadMarkers: function() {
		console.log('store is loaded. total records: ');
	}
})