Ext.define('ExApp.view.desktop.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',

    config: {
        tabBarPosition: 'top',
		defaults: {
			// style: 'background-color:#F00'
		},
		
        items: [
            {
            	title: 'Map',
            	iconCls: 'maps',
            	xtype: 'mapviewcontainer'
            },
            {
                title: 'List',
                iconCls: 'organize',
				xtype: 'parkinglistnav'
            },
            {
            	title: 'Settings',
            	iconCls: 'time',
            	xtype: 'timer'
            }
        ]
    }
});
