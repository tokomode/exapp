Ext.define('ExApp.view.Timer', {
	extend: 'Ext.Panel',
	xtype: 'timer',
	
	requires: [
		'Ext.field.Text',
		'Ext.field.DatePicker'
	],
	
	config: {
		layout: 'vbox',
		
		items: [
			{
				xtype: 'titlebar',
				docked: 'top',
				title: 'Timer'
			},
			{
				xtype: 'datepickerfield',
				name: 'time',
				margin: 10,
				label: 'Timer',
				value: new Date(),
				itemId: 'timerTextField'			
			},
			{
				xtype: 'button',
				margin: 10,
				text: 'Set Now',
				action: 'set'
			},
			{
				xtype: 'button',
				margin: 10,
				text: 'Reset Timer',
				action: 'reset'
			}
		]
	}
})