Ext.define('ExApp.model.Parking', {
	extend: 'Ext.data.Model',
	
	config: {
		fields: [
			{ name: 'id', type: 'string' },
			{ name: 'address', type: 'string' },
			{ name: 'rate', type: 'string' }, 
			{ name: 'lat', type: 'string' }, 
			{ name: 'lng', type: 'string' },
			//{ name: 'payment_methods', type: 'array' },
			// example for convert
			{ name: 'payment_methods', convert: paymentMethods },
			{ name: 'lot_type', mapping: 'carpark_type_str', type: 'string'},
			{ name: 'payment_options', type: 'string' },
			{ name: 'capacity', type: 'string' },
			{ name: 'max_height', type: 'string', convert: maxHeight },
			{ name: 'rate_details', mapping: 'rate_details.periods', type: 'array', convert: rateDetails },
			{ name: 'distance', type: 'integer', convert: calculateDistance } 
		]
	}
});

function paymentMethods(value, record) {
	return value.join(' | ');
}

function maxHeight(value, record) {
	if (value == 0) {
		return ' No height restriction';
	} else {
		return value;
	}
}

function rateDetails(value, record) {
	//console.dir(value);
	var result = "";
	for (var i = 0; i < value.length; i++) {
		result += '<div><strong>' + value[i].title + '</strong></div>';
		
		var rates = value[i].rates;
		for (var j = 0; j < rates.length; j++) {
			result += rates[j].when + ': ' + rates[j].rate + '<br />';
		}
	}
	return result;
}

function calculateDistance(value, record) {
	var parkingPoint = new google.maps.LatLng(record.data.lat, record.data.lng);
	var currentPoint = ExApp.app.globals.currentLocation;
	if (currentPoint == null) {
		console.log('*****Error: Current location is null at calculateDistance for ' + record.data.address);
		// return;
	}

	return Math.round(google.maps.geometry.spherical.computeDistanceBetween(parkingPoint, currentPoint));
}